package com.example.reactivewebapp.repository;

import com.example.reactivewebapp.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<User, String> {

    @Override
    Mono<User> findById(String id);
}
