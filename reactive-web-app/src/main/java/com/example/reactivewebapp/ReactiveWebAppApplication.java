package com.example.reactivewebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EnableReactiveMongoRepositories
@SpringBootApplication
public class ReactiveWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveWebAppApplication.class, args);
    }

}
